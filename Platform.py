# -*- coding: utf-8 -*-
"""
Created on Wed May 23 18:23:35 2018

@author: vh-edu
"""
"""
"""
import pygame, random, enemy
import Globals as gl
from Points import Points
from Sign import Sign


class Platform(pygame.sprite.Sprite):
    def __init__(self, width, height, position_x, position_y, list_patform):
        self.image = pygame.Surface([width, height])
        self.rect = self.image.get_rect()
        self.rect.x = position_x
        self.rect.y = position_y
        self.width = width
        self.height = height
        self.list_patform = list_patform
        self.index_list = []
        self.set_index_list()

    def set_index_list(self):
        if self.width > 100:
            self.index_list.append(random.randrange(0,4,1))
            for i in range(100,self.rect.width - 100, 100):
                self.index_list.append(random.randrange(4,8,1))
            self.index_list.append(random.randrange(8,12,1))
        else:
            self.index_list.append(random.randrange(12,16,1))


    def draw(self, surface):
        if self.index_list:
            surface.blit(self.list_patform[self.index_list[0]], self.rect)
            for i,j in enumerate(self.index_list[1:-1],1):
                surface.blit(self.list_patform[j], [self.rect.x+i*100,self.rect.y])
            surface.blit(self.list_patform[self.index_list[-1]],
                         [self.rect.right-100,self.rect.y])
        else:
            surface.blit(self.list_patform[self.index_list[0]], self.rect)

class MovingPlatform(Platform):
    def __init__(self, width, height, position_x, position_y, list_patform):
        super().__init__(width, height, position_x, position_y, list_patform)
        self.boundary_top = 0
        self.boundary_bottom = 0
        self.boundary_right = 0
        self.boundary_left = 0
        self.movement_x = 0
        self.movement_y = 0
        self.player = None


    def update(self):
        self.rect.x += self.movement_x
        if pygame.sprite.collide_rect(self, self.player):
            if self.movement_x > 0:
                self.player.rect.left == self.rect.right
            if self.movement_x < 0:
                self.player.rect.right == self.rect.left



        self.rect.y += self.movement_y
        if pygame.sprite.collide_rect(self, self.player):
            if self.movement_y > 0:
                self.player.rect.top = self.rect.bottom
            if self.movement_y < 0:
                self.player.rect.bottom = self.rect.top


        # sprawdzamy granice i decydujemy o zmianie kierunku
        position_y = self.rect.y - self.player.level.world_shift[1]
        if position_y > self.boundary_bottom \
           or position_y < self.boundary_top:
            self.movement_y *= -1

        position_x = self.rect.x - self.player.level.world_shift[0]
        if position_x < self.boundary_left or position_x > self.boundary_right:
            self.movement_x *= -1
            

class Solid_Platform(pygame.sprite.Sprite):
    def __init__(self, width, height, position_x, position_y, list_of_images):
        self.image = pygame.Surface([width, height])
        self.rect = self.image.get_rect()
        self.rect.x = position_x
        self.rect.y = position_y
        self.width = width
        self.height = height
        self.list_of_images = list_of_images
    
    def draw(self, surface):
        if self.width == 100:
            surface.blit(self.list_of_images[3])
        else:
            surface.blit(self.list_of_images[0], self.rect)
            for i in range(100, self.width - 100, 100):
                surface.blit(self.list_of_images[1], [self.rect.x + i,self.rect.y])

            surface.blit(self.list_of_images[2], [self.rect.right-100,self.rect.y])

class Wall(pygame.sprite.Sprite):
    def __init__(self, width, height, position_x, position_y, graphics):
        self.image = pygame.Surface([width, height])
        self.rect = self.image.get_rect()
        self.rect.x = position_x
        self.rect.y = position_y
        self.width = width
        self.height = height
        self.graphics = graphics
    
    def draw(self, surface):
        for i in range(0, self.height, 100):
            surface.blit(self.graphics, [self.rect.x,self.rect.y + i])



   
class Level:
    
    def __init__(self, player):
        self.player = player
        self.set_of_platforms = set()
        self.set_of_briars = set()
        self.set_of_enemies = pygame.sprite.Group()
        self.set_of_points = pygame.sprite.Group()
        self.set_of_minigames = pygame.sprite.Group()
        self.world_shift = [0,0]

    
    def update(self):

        for platform in self.set_of_platforms:
            platform.update()
        for briar in self.set_of_briars:
            briar.update()
            
        if self.player.rect.right >= (gl.game_screen_width//2):
            diff_x = self.player.rect.right - (gl.game_screen_width//2)
            self.player.rect.right = (gl.game_screen_width//2)
            self._shift_world(-diff_x,0)
        if self.player.rect.left <= (gl.game_screen_width//4):
            diff_x = (gl.game_screen_width//4)-self.player.rect.left
            self.player.rect.left = (gl.game_screen_width//4)
            self._shift_world(diff_x,0)
            
        if self.player.rect.top <= 200:
            diff_y = 200 - self.player.rect.top
            self.player.rect.top = 200
            self._shift_world(0,diff_y)
        if self.player.rect.bottom >= gl.game_screen_height - 350:
            diff_y = self.player.rect.bottom - (gl.game_screen_height - 350)
            self.player.rect.bottom = gl.game_screen_height - 350
            self._shift_world(0,-diff_y)

        
        self.set_of_points.update()
        self.set_of_enemies.update()
        self.set_of_minigames.update()


        
    def _shift_world(self, diff_x, diff_y):
        self.world_shift[0] += diff_x
        self.world_shift[1] += diff_y
        
        for platform in self.set_of_platforms:
            platform.rect.x += diff_x
            platform.rect.y += diff_y

        for briar in self.set_of_briars:
            briar.rect.x += diff_x
            briar.rect.y += diff_y

        for enemy in self.set_of_enemies:
            enemy.rect.x += diff_x
            enemy.rect.y += diff_y

        for point in self.set_of_points:
            point.rect.x += diff_x
            point.rect.y += diff_y


       # for game in self.set_of_minigames:
       #     game.rect.x += diff_x
       #     game.rect.y += diff_y



            
    def draw(self, surface):
        for platforms in self.set_of_platforms:
            platforms.draw(surface)
        for briar in self.set_of_briars:
            briar.draw(surface)

        
        self.set_of_points.draw(surface)
        self.set_of_enemies.draw(surface)
        self.set_of_minigames.draw(surface)

            

class Level1(Level):
    def __init__(self, player):
        super().__init__(player)

        #tworzymy zwykłe platformy:
        ws_platform = [
                        [300, 61, 300, 700],
                       [600,61,1400,300],
                       [600, 61, 2000, -100],
                        [400, 61, 6400, gl.game_screen_height - 900],

                    ]
        for el in ws_platform:
            platform = Platform(*el, gl.hanging_platform)
            self.set_of_platforms.add(platform)
            
        #minigra - przeszukiwanie w głąb
        platform = Platform(*[500,61,4400,-100], gl.hanging_platform)
        self.set_of_platforms.add(platform)
        minigra1 = Sign(gl.minigra1_image, platform)
        self.set_of_minigames.add(minigra1)
        
        platform = Platform(*[500, 61, 7200, gl.game_screen_height - 1300], gl.hanging_platform)
        self.set_of_platforms.add(platform)
        minigra2 = Sign(gl.minigra2_image, platform)
        self.set_of_minigames.add(minigra2)
        
        # tworzymy platforme z losowym wrogiem:
        ws_enemy_platform = [ [400,61,850,740],
                             [600, 61, 1600, 700],
                             [600, 61, 2500, 700],
                             [700, 61, 7000, gl.game_screen_height - 300]
                             ]
        for el in ws_enemy_platform:
            platform = Platform(*el, gl.hanging_platform)
            self.set_of_platforms.add(platform)

            if random.choice([1, 0]):
                platform_enemy = enemy.Platform_Enemy(gl.enemyA_right, gl.enemyA_left,
                                                      None, None, platform, random.choice([-3, -2, -1, 1, 2, 3]))
            else:
                platform_enemy = enemy.Platform_Enemy(gl.enemyB_right, gl.enemyB_left,
                                                      None, None, platform, random.choice([-3, -2, -1, 1, 2, 3]))

            point = Points(platform)
            self.set_of_points.add(point)
            self.set_of_enemies.add(platform_enemy)

        # tworzymy platformy z długim wrogiem
        ws_enemy_tall_platform = [

        ]
        for el in ws_enemy_tall_platform:
            platform = Platform(*el, gl.hanging_platform)
            self.set_of_platforms.add(platform)

            platform_enemy = enemy.Platform_Enemy(gl.enemyB_right, gl.enemyB_left,
                                                  None, None, platform, random.choice([-3, -2, -1, 1, 2, 3]))

            point = Points(platform)
            self.set_of_points.add(point)

            self.set_of_enemies.add(platform_enemy)

        # tworzymy platformy z małym wrogiem
        ws_enemy_small_platform = [
            [400, 61, 100, -100],
            [800, 61, 1800, -100],

        ]
        for el in ws_enemy_small_platform:
            platform = Platform(*el, gl.hanging_platform)
            self.set_of_platforms.add(platform)

            platform_enemy = enemy.Platform_Enemy(gl.enemyA_right, gl.enemyA_left,
                                                  None, None, platform, random.choice([-3, -2, -1, 1, 2, 3]))

            point = Points(platform)
            self.set_of_points.add(point)

            self.set_of_enemies.add(platform_enemy)

        # tworzymy ruchomą platformę (ruch w poziomie)
        mp_x = MovingPlatform(100, 61, 700, -100, gl.hanging_platform)
        mp_x.boundary_left = 600
        mp_x.boundary_right = 1300
        mp_x.movement_x = 2
        mp_x.player = self.player
        self.set_of_platforms.add(mp_x)

        # tworzymy ruchomą platformę (ruch w poziomie)
        mp_x = MovingPlatform(300, 61, 3300, gl.game_screen_height-700, gl.hanging_platform)
        mp_x.boundary_left = 3100
        mp_x.boundary_right = 3700
        mp_x.movement_x = 4
        mp_x.player = self.player
        point = Points(mp_x)
        self.set_of_points.add(point)
        self.set_of_platforms.add(mp_x)

        # tworzymy ruchomą platformę (ruch w pionie)
        mp_y = MovingPlatform(200, 61, 4100, 600, gl.hanging_platform)
        mp_y.boundary_top = -200
        mp_y.boundary_bottom = 1000
        mp_y.movement_y = 3
        mp_y.player = self.player
        point = Points(mp_y)
        self.set_of_points.add(point)
        self.set_of_platforms.add(mp_y)

        # tworzymy ruchomą platformę (ruch w poziomie)
        mp_x = MovingPlatform(300, 61, 5100, gl.game_screen_height - 900, gl.hanging_platform)
        mp_x.boundary_left = 5000
        mp_x.boundary_right = 5700
        mp_x.movement_x = 2
        mp_x.player = self.player
        point = Points(mp_x)
        self.set_of_points.add(point)
        self.set_of_platforms.add(mp_x)

        # tworzymy ruchomą platformę (ruch w poziomie)
        mp_x = MovingPlatform(300, 61, 6200, gl.game_screen_height - 500, gl.hanging_platform)
        mp_x.boundary_left = 5800
        mp_x.boundary_right = 6700
        mp_x.movement_x = 2
        mp_x.player = self.player
        point = Points(mp_x)
        self.set_of_points.add(point)
        self.set_of_platforms.add(mp_x)






        # tworzymy platformy ciągłe - dół
        ws_solid_bottom = [[1100,100,-500,gl.game_screen_height+100],
                           [1100,100,-500,gl.game_screen_height+200],
                           [1100,100,-500,gl.game_screen_height+300],
                           [600,100,2000,gl.game_screen_height+100],
                           [600,100,2000,gl.game_screen_height+200],
                           [600,100,2000,gl.game_screen_height+300],
                           [600,100,3300,gl.game_screen_height+100],
                           [600,100,3300,gl.game_screen_height+200],
                           [600,100,3300,gl.game_screen_height+300],
                           [1000,100,4700,gl.game_screen_height+100],
                           [1000,100,4700,gl.game_screen_height+200],
                           [1000,100,4700,gl.game_screen_height+300],
                            #solid po kolcami
                           

                           ]
        for el in ws_solid_bottom:
            platform = Solid_Platform(*el, gl.solid_bottomM)
            self.set_of_platforms.add(platform)

        ws_briar_bottom = [
                        [1400, 100, 600, gl.game_screen_height + 300],
                        [700, 100, 2600, gl.game_screen_height + 300],
                        [800, 100, 3900, gl.game_screen_height + 300],
                        [2000, 100, 5700, gl.game_screen_height + 300],
                        [1400, 100, 600, gl.game_screen_height + 200],
                        [700, 100, 2600, gl.game_screen_height + 200],
                        [800, 100, 3900, gl.game_screen_height + 200],
                        [2000, 100, 5700, gl.game_screen_height + 200],
                        [1400, 100, 600, gl.game_screen_height + 100],
                        [700, 100, 2600, gl.game_screen_height + 100],
                        [800, 100, 3900, gl.game_screen_height + 100],
                        [2000, 100, 5700, gl.game_screen_height + 100],
                        ]
        for el in ws_briar_bottom:
            platform = Platform(*el, gl.briarB)
            self.set_of_platforms.add(platform)

        ws_briar_top = [
                        [1400, 84, 600, gl.game_screen_height +16],
                        [700, 84, 2600, gl.game_screen_height +16],
                        [800, 84, 3900, gl.game_screen_height +16],
                        [2000, 84, 5700, gl.game_screen_height +16],
                        
                        ]

        for el in ws_briar_top:
            platform = Platform(*el, gl.briarT)
            self.set_of_briars.add(platform)

        







        # tworzymy duże platformy na dole
        ws_big_platform_b = [[600,100,2000,gl.game_screen_height],
                             [600,100,3300,gl.game_screen_height],
                             [1000,100,4700,gl.game_screen_height]
                             ]
        for el in  ws_big_platform_b:
            platform = Platform(*el, gl.main_platform_bottom)
            self.set_of_platforms.add(platform)

            platform_enemy = enemy.Platform_Enemy(gl.enemyA_right, gl.enemyA_left,
                                                  None, None, platform, random.choice([-3, -2, -1, 1, 2, 3]))

            self.set_of_enemies.add(platform_enemy)
            

        # tworzymy duże platformy na dole (prawe zakończenie)
        ws_big_platform_bR = [[600,100,0,gl.game_screen_height]]
            

        # tworzymy platformy ciągłe - dół (środek)
        ws_solid_bottomM = [[500,100,-500,gl.game_screen_height]]
        for el in ws_solid_bottomM:
            platform = Solid_Platform(*el, gl.solid_bottomM)
            self.set_of_platforms.add(platform)

    

        # tworzymy platformy ciągłe - góra otwarte
        ws_solid_bottom = [[500,100,-500,-720],[8200,100,-500,-820]]
        for el in ws_solid_bottom:
            platform = Solid_Platform(*el, gl.solid_topM)
            self.set_of_platforms.add(platform)

        # tworzymy duże platformy na górze 
        ws_big_platform_t = [[7700,100,0,-720]]
                
        for el in ws_big_platform_t:
            platform = Platform(*el, gl.main_platform_topM)
            self.set_of_platforms.add(platform)




        for el in  ws_big_platform_bR:
            platform = Platform(*el, gl.main_platform_bottomR)
            self.set_of_platforms.add(platform)

        # tworzymy ścianę - lewą
        self.set_of_platforms.add(Wall(*[100,1700,-100,-620], gl.solid_top[2]))
        self.set_of_platforms.add(Wall(*[100,1700,-200,-620], gl.solid_top[1]))
        self.set_of_platforms.add(Wall(*[100,1700,-300,-620], gl.solid_top[1]))
        self.set_of_platforms.add(Wall(*[100,1700,-400,-620], gl.solid_top[1]))
        self.set_of_platforms.add(Wall(*[100,1700,-500,-620], gl.solid_top[1]))

        # tworzymy ścianę - prawa
        #self.set_of_platforms.add(Wall(*[100, 1700, 8696, -520], gl.solid_top[1]))
        self.set_of_platforms.add(Wall(*[100, 2000, 8596, -820], gl.solid_top[1]))
        self.set_of_platforms.add(Wall(*[100, 2000, 8496, -820], gl.solid_top[1]))
        self.set_of_platforms.add(Wall(*[100, 2000, 8396, -820], gl.solid_top[1]))
        self.set_of_platforms.add(Wall(*[100, 2000, 8296, -820], gl.solid_top[1]))
        self.set_of_platforms.add(Wall(*[100, 2000, 8196, -820], gl.solid_top[1]))
        self.set_of_platforms.add(Wall(*[100, 2000, 8096, -820], gl.solid_top[1]))
        self.set_of_platforms.add(Wall(*[100, 2000, 7996, -820], gl.solid_top[1]))
        self.set_of_platforms.add(Wall(*[100, 2000, 7896, -820], gl.solid_top[1]))
        self.set_of_platforms.add(Wall(*[100, 2000, 7796, -820], gl.solid_top[1]))
        self.set_of_platforms.add(Wall(*[100, 2000, 7696, -820], gl.solid_topL[0]))

        gl.total_points += len(self.set_of_points)
        gl.total_points += len(self.set_of_minigames)*10
        


    def draw(self,surface):
        surface.blit(gl.level1bg,[(self.world_shift[0]-500),
                                  (self.world_shift[1]-700)//2])
        super().draw(surface)


