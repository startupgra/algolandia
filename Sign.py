import pygame
import Globals as gl

class Sign(pygame.sprite.Sprite):
    def __init__(self, image, platform):
        super().__init__()
        self.image = image
        self.rect = self.image.get_rect()
        self.platform = platform
        
        if self.platform:
            self.rect.bottom = self.platform.rect.top
            self.rect.centerx = self.platform.rect.centerx



    def update(self):
        super().update()
        if self.platform:
            self.rect.bottom=self.platform.rect.top
            self.rect.centerx =self.platform.rect.centerx


