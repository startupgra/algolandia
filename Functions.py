# -*- coding: utf-8 -*-
"""
Created on Wed May 23 13:06:26 2018

@author: vh-edu
"""
import pygame, sys, os
import Globals as gl
from Text import Text
import Platform
from Gracz import Player

#from Button import Button
#
#def sample_level():
#    game_exit_button = Button(
#                position=(gl.game_screen_width-50,0),
#                 width=0.2*gl.game_screen_width,height=0.1*gl.game_screen_height,
#                 text='Exit', 
#                 function=('game_exit',None),
#                 background = pygame.color.THECOLORS['darkred'],
#                 on_hover = pygame.color.THECOLORS['red'],
#                 font_size = 30,
#                 text_color = pygame.color.THECOLORS['black'],
#                )
#    game_exit_button.place('true')
#    while not game_exit:
#        gl.game_screen.blit(gl.sample_levelbg, [0,0])
#

#close function
def close():
    pygame.quit()
    sys.exit()

def resume():
    if gl.pause:
        gl.pause = False
    else:
        pass

def exit_game():
    if gl.game_exit is False:
        gl.pause = False
        gl.game_exit = True
    else:
        pass

def start_game():
    if gl.game_menu is True:
        gl.player1 = Player(gl.player_stand_list,gl.player_walk_right, gl.player_walk_left,
                 gl.player_jump_list_right, gl.player_jump_list_left, gl.player_dead_right, gl.player_dead_left)
        gl.player1.rect.bottom = 0
        gl.player1.rect.left = 00
        gl.current_level = Platform.Level1(gl.player1)
        gl.player1.level = gl.current_level
    

        gl.game_menu = False


def game_difficulty():
    gl.game_menu = False
    while not gl.game_menu:
        level = 1
        for event in pygame.event.get():
            # czerwony krzyrzyk spowoduje wymuszone zamknięcie gry
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
        gl.game_screen.blit(gl.menu_background, [0, 0])
        back = Button(None, ('game_menu', None),
                      os.path.join(os.getcwd(), 'img', 'left_arrow.png'),
                      os.path.join(os.getcwd(), 'img', 'left_arrow_green.png'),
                      (50, 50)
                      )
        back.place()

        difficulties = Text('DIFFICULTIES', (gl.WIDTH // 2, gl.HEIGHT * 0.1),
                            80, pygame.color.THECOLORS['red'])
        difficulties.draw(gl.game_screen, 'center')

        actual = Button("Mode: " + gl.difficulty, None,
                        pygame.color.THECOLORS['cyan'],
                        pygame.color.THECOLORS['cyan'],
                        (gl.WIDTH // 2, gl.HEIGHT * 0.2),
                        50,
                        pygame.color.THECOLORS['black'],
                        gl.WIDTH * 0.5,
                        gl.HEIGHT * 0.09)
        actual.place('center')

        easy = Button("EASY", ("difficulty_set", 'easy'),
                      pygame.color.THECOLORS['darkgreen'],
                      pygame.color.THECOLORS['green'],
                      (gl.WIDTH // 2, gl.HEIGHT * 0.3),
                      80,
                      pygame.color.THECOLORS['black'],
                      gl.WIDTH * 0.4,
                      gl.HEIGHT * 0.1)
        easy.place('center')

        medium = Button("MEDIUM", ("difficulty_set", 'medium'),
                        pygame.color.THECOLORS['yellow3'],
                        pygame.color.THECOLORS['yellow'],
                        (gl.WIDTH // 2, gl.HEIGHT * 0.5),
                        80,
                        pygame.color.THECOLORS['black'],
                        gl.WIDTH * 0.4,
                        gl.HEIGHT * 0.1)
        medium.place('center')

        hard = Button("HARD", ("difficulty_set", 'hard'),
                      pygame.color.THECOLORS['orangered2'],
                      pygame.color.THECOLORS['orangered'],
                      (gl.WIDTH // 2, gl.HEIGHT * 0.7),
                      80,
                      pygame.color.THECOLORS['black'],
                      gl.WIDTH * 0.4,
                      gl.HEIGHT * 0.1)
        hard.place('center')

        god = Button("GOD", ("difficulty_set", 'GOD'),
                     pygame.color.THECOLORS['darkred'],
                     pygame.color.THECOLORS['red'],
                     (gl.WIDTH // 2, gl.HEIGHT * 0.9),
                     80,
                     pygame.color.THECOLORS['black'],
                     gl.WIDTH * 0.4,
                     gl.HEIGHT * 0.1)
        god.place('center')
        pygame.display.update()
        # ustawianie zegaru na 60 odświeżeń na sekundę
        gl.clock.tick(20)


def game_menu():
    gl.game_menu = True


def difficulty_set(diff):
    gl.difficulty = diff[0]
    display = Button("Difficulty set", None,
               pygame.color.THECOLORS['white'],
               pygame.color.THECOLORS['white'],
               (gl.WIDTH//2, gl.HEIGHT//2),100,
               pygame.color.THECOLORS['black'],
              gl.WIDTH,
               100)
    display.place('center','sleep')
