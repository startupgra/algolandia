import random
import Globals as gl
import pygame, os

import Text as t




# grafika
background = pygame.image.load(os.path.join('img', 'game1', 'maze.png')).convert()
black_plate = pygame.image.load(os.path.join('img', 'game1', 'black.png')).convert_alpha(background)
rock_plate = pygame.image.load(os.path.join('img', 'game1', 'rock.png')).convert_alpha(background)
yellow_plate = pygame.image.load(os.path.join('img', 'game1', 'yellow.png')).convert_alpha(background)
player_1 = pygame.image.load(os.path.join('img', 'game1', 'player_1.png')).convert_alpha(background)
player_2 = pygame.image.load(os.path.join('img', 'game1', 'player_2.png')).convert_alpha(background)
player_3 = pygame.image.load(os.path.join('img', 'game1', 'player_3.png')).convert_alpha(background)
player_4 = pygame.image.load(os.path.join('img', 'game1', 'player_4.png')).convert_alpha(background)

player_image = [player_1,player_2,player_3,player_4]

class Plate(pygame.sprite.Sprite):
    def __init__(self, image_list, rect_x, rect_y, coordinates):
        super().__init__()
        self.image_list = image_list
        self.image = self.image_list[0]
        self.rect = self.image.get_rect()
        self.covered = True
        self.rect.x = rect_x
        self.rect.y = rect_y
        self.coordinates = coordinates

    def draw(self, surface):
        if not self.covered:
            self.image = self.image_list[1]
        else:
            self.image = self.image_list[0]
        surface.blit(self.image, self.rect)

class Player(pygame.sprite.Sprite):
    def __init__(self, image_list):
        self.image_list = image_list
        self.image = self.image_list[0]
        self.rect = self.image.get_rect()
        self.rect.center = [410, 270]
        self.coordinates = (1,0)
        pygame.event.clear()
        #pygame.time.delay(500)
        

        
        

class Game:
    def __init__(self, player, maze_string):
        self.set_of_plates = set()
        self.player = player
        self.maze_table = self._set_maze_table(maze_string)
        self.yellow_path = []
        self._set_attributes()

        for i in range(len(self.maze_table)):
            for j in range(len(self.maze_table[i])):
                if self.maze_table[i][j] == '@':
                    plate = Plate([black_plate, yellow_plate], 380 + j*60, 180 + i*60, (i,j))
                    self.yellow_path.append((i,j))
                else:
                    plate = Plate([black_plate, rock_plate], 380 + j*60, 180 + i*60, (i,j))
                
                self.set_of_plates.add(plate)

        #eksperyment
##        for x in self.set_of_plates:
##            x.covered = False

        #print(self.path)
                    
    def _set_attributes(self):
        self.path = self._set_path(self.maze_table)
        # dodajemy dwukrotnie pole z indeksem (8,17) żeby w ostatnim kroku self.path[-1] nie zwracało błędu
        self.path.append((8,17))
        self.path.reverse()
        self.discovered_path = [self.path.pop()]
        
        

    def draw(self, surface):
        surface.blit(background, [0,0])
        #screen.fill(pygame.color.THECOLORS['lightblue'])
        for plate in self.set_of_plates:
            plate.draw(surface)

        surface.blit(self.player.image, self.player.rect)

        
    def update(self):
        cr = self.player.coordinates
        cr_list = [cr, (cr[0],cr[1]+1), (cr[0]+1,cr[1]), (cr[0],cr[1]-1), (cr[0]-1,cr[1])]
        for plate in self.set_of_plates:
            if plate.coordinates in cr_list:
                plate.covered = False

##        if cr in self.discovered_path or cr == self.path[-1]:
##            if cr not in self.discovered_path:
##                self.discovered_path.append(self.path.pop())
##        else:
##            self._set_attributes()
##            self.player.coordinates = (1,0)
##            self.player.rect.center = [340, 220]
##            self.player.image = self.player.image_list[0]
##            for plate in self.set_of_plates:
##                plate.covered = True


    def up_player(self):
        cr = (self.player.coordinates[0]-1, self.player.coordinates[1])
        if cr in self.yellow_path:
            self.player.coordinates = cr
            self.player.rect.y -= 60
            self.player.image = self.player.image_list[3]

    def down_player(self):
        cr = (self.player.coordinates[0]+1, self.player.coordinates[1])
        if cr in self.yellow_path:
            self.player.coordinates = cr
            self.player.rect.y += 60
            self.player.image = self.player.image_list[1]

    def right_player(self):
        cr = (self.player.coordinates[0], self.player.coordinates[1]+1)
        if cr in self.yellow_path:
            self.player.coordinates = cr
            self.player.rect.x += 60
            self.player.image = self.player.image_list[0]

    def left_player(self):
        cr = (self.player.coordinates[0], self.player.coordinates[1]-1)
        if cr in self.yellow_path:
            self.player.coordinates = cr
            self.player.rect.x -= 60
            self.player.image = self.player.image_list[2]

    def get_event_player(self,event):
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP:
                self.up_player()
            if event.key == pygame.K_DOWN:
                self.down_player()
            if event.key == pygame.K_RIGHT:
                self.right_player()
            if event.key == pygame.K_LEFT:
                self.left_player()
            
            
    def _set_maze_table(self, maze_string):
        tab = [];
        line = []
        for ch in maze_string:
            if ch != '\n':
                line.append(ch)
            else:
                line = []
                tab.append(line)

        return tab

    def _set_path(self, maze_table):
        start = (1,0)
        stop = (8,17)
        
        path = [start]
        stack = [start]

        while stop not in path:
            position = stack[-1]
            i =  position[0]
            j =  position[1]
            if 0 <= i < 10 and 0 <= j < 17 and maze_table[i][j+1] == '@' and (i,j+1) not in path:
                path.append((i,j+1))
                stack.append((i,j+1))
            elif 0 <= i < 10 and 0 <= j < 17 and maze_table[i+1][j] == '@' and (i+1,j) not in path:
                path.append((i+1,j))
                stack.append((i+1,j))
            elif 0 <= i < 10 and 0 <= j < 17 and maze_table[i][j-1] == '@' and (i,j-1) not in path:
                path.append((i,j-1))
                stack.append((i,j-1))
            elif 0 <= i < 10 and 0 <= j < 17 and maze_table[i-1][j] == '@' and (i-1,j) not in path:
                path.append((i-1,j))
                stack.append((i-1,j))
            else:
                stack.pop()

        return path
        
        
class Text:
    def __init__(self, text, text_colour,size = 74):
        self.text = text
        self.text_colour = text_colour
        self.font = pygame.font.SysFont(None, size)
        self.image = self.font.render(str(self.text), 1, self.text_colour)
        self.rect = self.image.get_rect()

    def draw(self, surface):
        surface.blit(self.image, self.rect)
    
 

L1 = """
XXXXXXXXXXXXXXXXXX
@@@@@@XXX@X@@@@@XX
X@X@X@X@@@X@X@XXXX
X@X@XXXXX@@@XXX@XX
X@X@@@@X@@X@X@@@@X
XXX@XX@X@XXX@X@XXX
X@@@@X@X@@@@@@@@@X
X@XX@X@XX@XXXX@X@X
X@@X@X@@@@@@X@@X@@
XXXXXXXXXXXXXXXXXX"""
L2 = """
XXXXXXXXXXXXXXXXXX
@@@@X@XXX@@@@X@@@X
XXX@X@@@@@XX@XXX@X
XXX@@@XXX@@X@@@@@X
XXXXX@XXXXXX@XXXXX
XX@@@@@@@@@X@@@@XX
XXXXXXXXXX@X@XX@@X
X@@@@@@@@@@X@XXX@X
XXXX@XXX@XXX@@X@@@
XXXXXXXXXXXXXXXXXX"""
L3 = """
XXXXXXXXXXXXXXXXXX
@@@@@XXXX@@@@XXXXX
X@XXXXX@@@XX@X@@@X
X@@@@@X@XXXX@X@X@X
X@XXXXX@XX@@@@@X@X
X@@@@@@@XXXX@X@X@X
X@XXXXX@XX@@@X@X@X
X@@@@XX@XXXXXX@XXX
X@XXXXX@@@@@XX@@@@
XXXXXXXXXXXXXXXXXX"""
L4 = """
XXXXXXXXXXXXXXXXXX
@@X@@@@@@@@@@@@@XX
X@X@X@XXX@XXX@XXXX
X@X@X@XXX@X@@@@@@X
X@X@X@XXX@@XX@XXXX
X@X@XXXXXX@XXXXXXX
X@@@@@@@XX@@@@@@@X
X@XXXXXXXX@XX@X@XX
X@@@@X@@@@@XX@X@@@
XXXXXXXXXXXXXXXXXX"""

game = Game(Player(player_image), L1)
restart = False
text1 = t.Text(text='To był zły ruch!',
             #position=(gl.game_screen.get_rect().centerx, gl.game_screen.get_rect().centery-70),
             font_size=int(3*((gl.game_screen_height+gl.game_screen_width)/100)),
             text_color=pygame.color.THECOLORS['yellow'])

text1.rect.center = gl.game_screen.get_rect().center
text1.rect.y -= 100
text2 = t.Text(text='Spróbuj ponownie.',
             #position=gl.game_screen.get_rect().center,
             font_size=int(3*((gl.game_screen_height+gl.game_screen_width)/100)),
             text_color=pygame.color.THECOLORS['yellow'])

#text2 = t.Text('Spróbuj ponownie.', pygame.color.THECOLORS['yellow'])
text2.rect.center = gl.game_screen.get_rect().center
text3 = t.Text(
    text='Gratulacje! Znalazłeś wyjście',
    font_size=int(3*((gl.game_screen_height+gl.game_screen_width)/100)),
    text_color=pygame.color.THECOLORS['yellow'])
text3.rect.center = gl.game_screen.get_rect().center

text4 = t.Text(text='+10 punktów',
             font_size=int(3*((gl.game_screen_height+gl.game_screen_width)/100)),
             text_color=pygame.color.THECOLORS['yellow'])
text4.rect.center = gl.game_screen.get_rect().center


###print(droga)
### głowna pętla gry
##window_open = True
##while window_open:
##    # pętla zdarzeń
##    for event in pygame.event.get():
##        if event.type == pygame.KEYDOWN:
##            if event.key == pygame.K_ESCAPE:
##                window_open = False
##        elif event.type == pygame.QUIT:
##            window_open = False
##
##        game.get_event_player(event)
##
##    game.update()
##    game.draw(screen)
##    cor = game.player.coordinates
##    if cor in game.discovered_path or cor == game.path[-1]:
##            if cor not in game.discovered_path:
##                game.discovered_path.append(game.path.pop())
##    else:
##        pygame.display.flip()
##        pygame.time.delay(500)
##        game._set_attributes()
##        game.player.coordinates = (1,0)
##        game.player.rect.center = [410, 270]
##        game.player.image = game.player.image_list[0]
##        for plate in game.set_of_plates:
##            plate.covered = True
##        restart = True
##        screen.blit(background, [0,0])
##        text1.draw(screen)
##        text2.draw(screen)
##        
##        
##            
##
##    if cor == (8,17):
##        # tu trzeba wrócić do głownej gry
##        pass
##        #print('Koniec mini gry');
##    
##    #aktualizacja okna pygame
##    pygame.display.flip()
##    if restart:
##        pygame.time.delay(3000)
##        restart = False
##    clock.tick(30)
##
##pygame.quit()



    
