import pygame, time, random, sys, os
import Globals as gl
from Text import Text
import Functions as fun
class Button:
    func = {}
    
    def __init__(self, 
                 position=(0,0),
                 width = 100,
                 height = 50,
                 text=None, 
                 function=None,
                 background = pygame.color.THECOLORS['darkgreen'],
                 on_hover = pygame.color.THECOLORS['green'],
                 font_size = 30,
                 text_color = pygame.color.THECOLORS['black'],
                 font = 'chiller'):
        self.text = text
        self.function = function
        self.background = background
        self.background_image = False
        self.on_hover = on_hover
        self.position = position
        self.font_size = font_size
        self.text_color = text_color
        if font in pygame.font.get_fonts():
            self.font = pygame.font.SysFont(font, font_size)
        else:
            self.font = pygame.font.Font(os.path.join(os.getcwd(), 'fonts',font), font_size)

        #self.parametry = []
        #przycisk z obrazkiem w tle(width, height - obrazka)
        if type(background) is str:
            self.background = pygame.image.load(os.path.join(os.getcwd(), 'img',background))
            self.background_rect = self.background.get_rect()
            self.background_rect.center = self.position
            self.background_image = True
        elif type(background) is pygame.Surface:
            self.background_rect = self.background.get_rect()
            self.background_rect.center = self.position
            self.background_image = True
        else:
            self.width = width
            self.height = height

        #
        if type(on_hover) is str:
            self.on_hover = pygame.image.load(on_hover)
            self.on_hover_rect = self.on_hover.get_rect()
            self.on_hover_rect.center = self.position
        elif type(on_hover) is pygame.Surface: 
            self.on_hover_rect = self.on_hover.get_rect()
            self.on_hover_rect.center = self.position

    def place(self, center=None, sleep=None):
        mouse = pygame.mouse.get_pos()
        click = pygame.mouse.get_pressed()
        self.parametry = []
        if center:
        #position - centr button'a
            if self.background_image:
                if self.background_rect.x < mouse[0] < self.background_rect.x+self.background_rect.width and self.background_rect.y < mouse[1] < self.background_rect.y+self.background_rect.height:
                    gl.game_screen.blit(self.on_hover,self.on_hover_rect)
                    if self.function is not None:
                        if click[0]:
                            if self.function[1]:
                                for i in range(1,len(self.function)):
                                    self.parametry.append(self.function[i])
                                Button.func[self.function[0]](self.parametry)
                            else:
                                Button.func[self.function[0]]()
                                    
                                    
                    if self.text:
                        text = Text(self.text,
                                self.on_hover_rect.center,
                                self.font_size, self.text_color, self.font)
                        text.draw(gl.game_screen, 'center')
                else:
                    gl.game_screen.blit(self.background,self.background_rect)
                    if self.text:
                        text = Text(self.text,
                                self.background_rect.center,
                                self.font_size, self.text_color, self.font)
                        text.draw(gl.game_screen, 'center')
            #rysowanie prostokątu
            else:
                if self.position[0]-self.width//2 < mouse[0] < self.position[0]-self.width//2+self.width and self.position[1]-self.height//2 < mouse[1] < self.position[1]-self.height//2+self.height:
                    cent = pygame.draw.rect(gl.game_screen, self.on_hover,
                                    (self.position[0]-self.width//2,self.position[1]-self.height//2,self.width, self.height))
                    
                    #wykonanie funkcji po wciśnięciu
                    if self.function is not None:
                        if click[0]:
                            if self.function[1]:
                                for i in range(1,len(self.function)):
                                    self.parametry.append(self.function[i])
                                Button.func[self.function[0]](self.parametry)
                            else:
                                Button.func[self.function[0]]()
                    if self.text:
                        text = Text(self.text,
                                cent.center,
                                self.font_size, self.text_color, self.font)
                        text.draw(gl.game_screen, 'center')
                else:
                    cent = pygame.draw.rect(gl.game_screen, self.background,
                                    (self.position[0]-self.width//2,self.position[1]-self.height//2,self.width, self.height))
                    if self.text:
                        text = Text(self.text,
                                cent.center,
                                self.font_size, self.text_color, self.font)
                        text.draw(gl.game_screen, 'center')
                    
            
                
        #position - star cordinates of button       
        else:
            if self.background_image:
                if self.background_rect.x < mouse[0] < self.background_rect.x+self.background_rect.width and self.background_rect.y < mouse[1] < self.background_rect.y+self.background_rect.height:
                    gl.game_screen.blit(self.on_hover,self.on_hover_rect)
                    if self.function is not None:
                        if click[0]:
                            if self.function[1]:
                                for i in range(1,len(self.function)):
                                    self.parametry.append(self.function[i])
                                Button.func[self.function[0]](self.parametry)
                            else:
                                Button.func[self.function[0]]()
                    if self.text:
                        text = Text(self.text,
                                self.on_hover_rect.center,
                                self.font_size, self.text_color, self.font)
                        text.draw(gl.game_screen, 'center')
                else:
                    gl.game_screen.blit(self.background,self.background_rect)
                    if self.text:
                        text = Text(self.text,
                                self.background_rect.center,
                                self.font_size, self.text_color, self.font)
                        text.draw(gl.game_screen, 'center')
            #malowanie prostokątu
            else:
                if self.position[0]-self.width//2 < mouse[0] < self.position[0]-self.width//2+self.width and self.position[1]-self.height//2 < mouse[1] < self.position[1]-self.height//2+self.height:
                    cent = pygame.draw.rect(gl.game_screen, self.on_hover,
                                    (self.position[0]-self.width//2,self.position[1]-self.height//2,self.width, self.height))

                    
                    #wykonanie funkcji po wciśnięciu
                    if self.function is not None:
                            if click[0]:
                                if self.function[1]:
                                    for i in range(1,len(self.function)):
                                        self.parametry.append(self.function[i])
                                    Button.func[self.function[0]](self.parametry)
                                else:
                                    Button.func[self.function[0]]()

                    if self.text:
                            text = Text(self.text,
                                    self.cent.center,
                                    self.font_size, self.text_color, self.font)
                            text.draw(gl.game_screen, 'center')
        
                    

    #func['sample_level'] = fun.sample_level
    func['close'] = fun.close
    #continue function

    
    func['resume'] = fun.resume
       
    
    
    
    #exit function

    func['exit_game'] = fun.exit_game

    
    


    func['start_game'] = fun.start_game


    
    func['game_difficulty'] = fun.game_difficulty

    func['game_menu'] = fun.game_menu
        
        

    
    func['difficulty_set']= fun.difficulty_set

    def main_menu():
        gl.game_start = False
        gl.game_exit=True
        gl.game_menu = True
        gl.pause = False
    func['main_menu'] = main_menu
            
            
