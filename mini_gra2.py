import random

import pygame, os
import Text as t
import Globals as gl
##pygame.init()


## ustawienia ekranu i gry
##screen = pygame.display.set_mode((0,0),pygame.FULLSCREEN)
##clock = pygame.time.Clock()


# grafika
background = pygame.image.load(os.path.join('img', 'game2', 'chest.png')).convert()
frame_image = pygame.image.load(os.path.join('img', 'game2', 'frame.png'))
files_of_letters = sorted(os.listdir(os.path.join('img','game2')))

images_of_letters = [pygame.image.load(os.path.join('img','game2', name)).convert_alpha(background)
                   for name in files_of_letters  if "letter" in name]

button_right = [pygame.image.load(os.path.join('img','game2', name)).convert_alpha(background)
                   for name in files_of_letters  if "button_R" in name]

button_left = [pygame.image.load(os.path.join('img','game2', name)).convert_alpha(background)
                   for name in files_of_letters  if "button_L" in name]

button_round = [pygame.image.load(os.path.join('img','game2', name)).convert_alpha(background)
                   for name in files_of_letters  if "button_O" in name]


class Letter(pygame.sprite.Sprite):
    def __init__(self, image,name, frame):
        super().__init__()
        self.image = image
        self.rect = self.image.get_rect()
        self.name = name
        self.position = 0
        self.boundaryR = None
        self.boundaryL = None
        self.movement_x = 0
        self.frame = frame

    def draw(self, surface):
        surface.blit(self.image, self.rect)

    def run(self):
        if self.frame.position % 2 == 0: 
            if self.position % 2 == 0:
                self.boundaryR = self.rect.right
                self.movement_x = 5
            else:
                self.movement_x = -5
                self.boundaryL = self.rect.left
        else:
            if self.position % 2 == 1:
                self.boundaryR = self.rect.right
                self.movement_x = 5
            else:
                self.movement_x = -5
                self.boundaryL = self.rect.left            

    def update(self):
        self.rect.x += self.movement_x
        if self.movement_x > 0:
            if self.rect.left > self.boundaryR:
                self.rect.left = self.boundaryR
                self.position += 1
                self.movement_x = 0  
        if self.movement_x < 0:
            if self.rect.right < self.boundaryL:
                self.rect.right = self.boundaryL
                self.position -= 1
                self.movement_x = 0           
            
        

class Frame(pygame.sprite.Sprite):
    def __init__(self, image):
        self.image = image
        self.rect = self.image.get_rect()
        self.position = 0

        
class Button:
    def __init__(self, image_list, center):
        self.center = center
        self.image_list = image_list
        self.image = image_list[0]
        self.rect = self.image.get_rect()
        self.rect.center = self.center

    def draw(self, surface):
        surface.blit(self.image, self.rect)
        
        

class Game:
    def __init__(self, frame):
        self.frame = frame
        self.frame.rect.x = 533
        self.frame.rect.y = 179
        self.alphabet = [Letter(images_of_letters[index], name, self.frame)
                         for index, name in enumerate('ABCDEFGHJKNOPRSXZ',0)]
        self.letters = []

        while len(self.letters) < 10:
            letter = random.choice(self.alphabet)
            if letter not in self.letters:
                self.letters.append(letter)





        self.buttonL = Button(button_left, [600,400])
        self.buttonR = Button(button_right, [800,400])
        self.buttonO = Button(button_round, [1450,400])
        self.order_bubble_sort_list = None
        self.find_bubble_sort_list()
        self.game_ok = True
        self.last = None


        pygame.event.clear()
        
            


    def draw(self, surface):
        surface.blit(background, [0,0])
        for letter in self.letters:
            letter.draw(surface)
        
        surface.blit(self.frame.image, self.frame.rect)
        surface.blit(self.buttonR.image, self.buttonR.rect)
        surface.blit(self.buttonL.image, self.buttonL.rect)
        surface.blit(self.buttonO.image, self.buttonO.rect) 

    def find_bubble_sort_list(self):
        for i, letter in enumerate(self.letters,0):
            letter.rect.y = 182
            letter.rect.x = 537 + 100 * i
            letter.position = i
        
        order_list = []
        letters = [ob.name for ob in self.letters]
        i = len(letters)
        while i > 0:
            for j in range(i - 1):
                if letters[j] > letters[j+1]:
                    order_list.append(j)
                    letters[j], letters[j+1] = letters[j+1], letters[j]
            i -= 1
                
        order_list.append(-1)
        order_list.reverse()
        self.order_bubble_sort_list = order_list

        

    def update(self):
        for letter in self.letters:
            letter.update()



    def right_frame(self):
        if self.frame.position < 8:
            self.frame.position += 1
            self.frame.rect.x += 100

    def left_frame(self):
        if 0 < self.frame.position:
            self.frame.position -= 1
            self.frame.rect.x -= 100

    def find_letter(self, position):
        for letter in self.letters:
            if letter.position == position:
                return letter
    
    def get_event(self,event):
        if event.type == pygame.MOUSEMOTION:
            if self.buttonL.rect.collidepoint(pygame.mouse.get_pos()):
                self.buttonL.image = self.buttonL.image_list[1]
                self.buttonL.rect = self.buttonL.image.get_rect()
                self.buttonL.rect.center = self.buttonL.center
            else:
                self.buttonL.image = self.buttonL.image_list[0]
                self.buttonL.rect = self.buttonL.image.get_rect()
                self.buttonL.rect.center = self.buttonL.center

            if self.buttonR.rect.collidepoint(pygame.mouse.get_pos()):
                self.buttonR.image = self.buttonR.image_list[1]
                self.buttonR.rect = self.buttonR.image.get_rect()
                self.buttonR.rect.center = self.buttonR.center
            else:
                self.buttonR.image = self.buttonR.image_list[0]
                self.buttonR.rect = self.buttonR.image.get_rect()
                self.buttonR.rect.center = self.buttonR.center

            if self.buttonO.rect.collidepoint(pygame.mouse.get_pos()):
                self.buttonO.image = self.buttonO.image_list[1]
                self.buttonO.rect = self.buttonO.image.get_rect()
                self.buttonO.rect.center = self.buttonO.center
            else:
                self.buttonO.image = self.buttonO.image_list[0]
                self.buttonO.rect = self.buttonO.image.get_rect()
                self.buttonO.rect.center = self.buttonO.center

        if event.type == pygame.MOUSEBUTTONDOWN:
            if self.buttonO.rect.collidepoint(pygame.mouse.get_pos()) and\
            self.find_letter(self.frame.position).movement_x == 0:
                if self.order_bubble_sort_list[-1] != self.frame.position:
                    self.game_ok = False
                else:
                    self.find_letter(self.frame.position).run()
                    self.find_letter(self.frame.position+1).run()
                    x = self.order_bubble_sort_list[-1]
                    self.order_bubble_sort_list.pop()
                    if self.order_bubble_sort_list[-1] == -1:
                        self.last = x
            if self.buttonR.rect.collidepoint(pygame.mouse.get_pos()):
                self.right_frame()
            if self.buttonL.rect.collidepoint(pygame.mouse.get_pos()):
                self.left_frame()                
            





        
class Text:
    def __init__(self, text, text_colour, size = 74):
        self.text = text
        self.text_colour = text_colour
        self.font = pygame.font.SysFont(None, size)
        self.image = self.font.render(str(self.text), 1, self.text_colour)
        self.rect = self.image.get_rect()

    def draw(self, surface):
        surface.blit(self.image, self.rect)
    
 

game = Game(Frame(frame_image))
text1 = t.Text('To był zły ruch!',
                int(2*((gl.game_screen_height+gl.game_screen_width)/100)),
               pygame.color.THECOLORS['yellow'])
text1.rect.x = 1240
text1.rect.y = 680
text2 = t.Text('Sortuj od początku.',
             int(2*((gl.game_screen_height+gl.game_screen_width)/100)),
             pygame.color.THECOLORS['yellow'])
text2.rect.x = 1200
text2.rect.y = 750
text3 = t.Text('Gratulacje!!',
             int(3*((gl.game_screen_height+gl.game_screen_width)/100)),
             pygame.color.THECOLORS['yellow'])
text3.rect.x = 1240
text3.rect.y = 680
text4 = t.Text('+10 punktów',
             int(3*((gl.game_screen_height+gl.game_screen_width)/100)),
             pygame.color.THECOLORS['yellow'])
text4.rect.x = 1240
text4.rect.y = 680



# głowna pętla gry
##window_open = True
##while window_open:
##    # pętla zdarzeń
##    for event in pygame.event.get():
##        if event.type == pygame.KEYDOWN:
##            if event.key == pygame.K_ESCAPE:
##                window_open = False
##        elif event.type == pygame.QUIT:
##            window_open = False
##
##        game.get_event(event)
##
##    # tu koniec gry!!! (wyście do głównej gry)
##    if game.order_bubble_sort_list[-1] == -1 and\
##       game.find_letter(game.last).movement_x == 0:
##        
##        text3.draw(screen)
##        pygame.display.flip()
##        pygame.time.delay(3000)
##        break
##
##    if game.game_ok:
##        game.update()
##        game.draw(screen)
##    elif game.order_bubble_sort_list[-1] != -1:
##        pygame.time.delay(500)
##        screen.blit(background, [0,0])
##        text1.draw(screen)
##        text2.draw(screen)
##        random.shuffle(game.letters)
##        game.find_bubble_sort_list()
##        game.game_ok = True
##        game.frame.rect.x = 533
##        game.frame.rect.y = 179
##        game.frame.position = 0
##        pygame.display.flip()
##        pygame.time.delay(3000)
##        
##
##        
##        
##        
##    
##    #aktualizacja okna pygame
##    pygame.display.flip()
    ##clock.tick(30)

##pygame.quit()



    
